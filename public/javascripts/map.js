$(function() {
    const flippedYAxisCRS = L.extend({}, L.CRS.Simple, {
      transformation: new L.Transformation(1, 0, 1, 0)
    });

    // Create map
    const map = L.map('mapDemo', {
        minZoom: 1,
        maxZoom: 5,
        zoomSnap: 0.25,
        crs: flippedYAxisCRS
    });

    let layer = null;
    function setMapImage(image) {
        if (layer) {
            map.removeLayer(layer)
        }

        // Define map bounds so that it fits data coordinates
        const bounds = [[0, 0], [375, 500]];
        layer = L.imageOverlay(image, bounds)
        layer.addTo(map)
        map.fitBounds(bounds);
    }

    // Place Zoom Control at screen bottom right of the screen
    map.zoomControl.setPosition('bottomright');

    let floorButton = $('.floor-button')
    let imageUrl = floorButton.data('image')
    let floorId = floorButton.data('floor-id')
    // Try to find ground level image
    floorButton.each(function() {
        if ($(this).data('level') === 0) {
            imageUrl = $(this).data('image')
            floorId = $(this).data('floor-id')
        }
    })

    floorButton.on('click', function() {
        setMapImage($(this).data('image'))
        floorId = $(this).data('floor-id')
        addTacticsToMap()
    })

    setMapImage(imageUrl)

    const tacticTemplate = document.getElementById('popup-template').innerHTML;
    let lastLayer = null;
    let tactics = []

    $.ajax($('#mapData')
        .data('tactics-url'))
        .done(function(data) {
            tactics = data
            addTacticsToMap()
        });

    function bindTactic(feature, layer) {
        for (i=0; i < feature.properties.tactics.length; i++) {
            // Bind custom properties for each tactic, these can be used in the popup template
            if (feature.properties.tactics[i].index === 0) {
                // Set default active element for carousel
                feature.properties.tactics[i].active = "active"
            }

            console.log(feature.properties.tactics[i])
        }

        var rendered = Mustache.render(tacticTemplate, feature.properties)
        layer.bindPopup(rendered)
    }

    addTacticsToMap()
    function addTacticsToMap() {
        if (lastLayer) {
            map.removeLayer(lastLayer)
        }

        if (tactics[floorId]) {
            console.log("adding " + tactics[floorId].length + " tactic groups")

            lastLayer = L.geoJSON(tactics[floorId], {style: {"color": "#FF0000", "weight": 3, "opacity": 2}, onEachFeature: bindTactic})
            lastLayer.addTo(map)
        }
    }
});