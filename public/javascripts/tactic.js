$(function() {
    function resetInputField(node, attributes, newIndex) {
        for (let i = 0; i < attributes.length; i++) {
            if (node.attr(attributes[i]) !== undefined) {
                node.attr(attributes[i], node.attr(attributes[i]).replace(numberRegex, newIndex));

                if (node.attr("value")) {
                    node.attr('value', "")
                }
            }
        }
    }

    function updateNodeIndexNumbers(node, index) {
        node.children().each(function() {
            resetInputField($(this), ['id', 'name', 'for'], index)
            updateNodeIndexNumbers($(this), index);
        });
    }

    $(".add-tactic").on('click', function() {
        let index = $('.tactic-form').length

        updateNodeIndexNumbers(template, index)
        $('.tactics-container').append(template.prop('outerHTML'))

        bindRemoveFormEvents()
    });

    function bindRemoveFormEvents() {
        $('.remove-tactic').unbind('click.remove').bind('click.remove', function(){
            $(this).closest('.tactic-form').remove();
        });
    }

    const flippedYAxisCRS = L.extend({}, L.CRS.Simple, {
        transformation: new L.Transformation(1, 0, 1, 0)
    });

    function triggerMapFloorChange() {
        let optionElement = mapFloorSelectElement.children('option:selected')

        if (optionElement.length <= 0) {
            return
        }
        optionElement = optionElement.first();
        $('.current-map').text(optionElement.text())

        if (!optionElement.data('image')) {
            return
        }

        if (imageLayer) {
            map.removeLayer(imageLayer)
        }

        console.log("setting map layer " + optionElement.data('image'))

        // Define map bounds so that it fits data coordinates
        const bounds = [[0, 0], [375, 500]];
        imageLayer = L.imageOverlay(optionElement.data('image'), bounds)
        imageLayer.addTo(map)

        drawCoordinates();
    }

    function drawCoordinates() {
        if (myLayer !== null) {
            map.removeLayer(myLayer)
        }

        // Translates the rects information to coordinates and updates the input fields
        setCoordinatesFromRect()

        let feature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
            "type": "Polygon",
                "coordinates": [
                [
                    [
                        $('#tacticGroup_coordinates_0_x').val(),
                        $('#tacticGroup_coordinates_0_y').val()
                    ],
                    [
                        $('#tacticGroup_coordinates_1_x').val(),
                        $('#tacticGroup_coordinates_1_y').val()
                    ],
                    [
                        $('#tacticGroup_coordinates_2_x').val(),
                        $('#tacticGroup_coordinates_2_y').val()
                    ],
                    [
                        $('#tacticGroup_coordinates_3_x').val(),
                        $('#tacticGroup_coordinates_3_y').val()
                    ],
                ]
            ]}
        }

        myLayer = L.geoJSON(feature, {style: {"color": "#FF0000", "weight": 3, "opacity": 2}});

        map.addLayer(myLayer);
        map.fitBounds(myLayer.getBounds());
    }

    function setCoordinatesFromRect() {
        let x = parseInt($('#rect-x').val())
        let y = parseInt($('#rect-y').val())
        let width = parseInt($('#rect-width').val())
        let height = parseInt($('#rect-height').val())

        $('#tacticGroup_coordinates_0_x').val(x)
        $('#tacticGroup_coordinates_0_y').val(y)

        $('#tacticGroup_coordinates_1_x').val(x + width)
        $('#tacticGroup_coordinates_1_y').val(y)

        $('#tacticGroup_coordinates_2_x').val(x + width)
        $('#tacticGroup_coordinates_2_y').val(y + height)

        $('#tacticGroup_coordinates_3_x').val(x)
        $('#tacticGroup_coordinates_3_y').val(y + height)
    }

    function setInitialRectFromCoordinates() {
        let x = parseInt($('#tacticGroup_coordinates_0_x').val())
        let y = parseInt($('#tacticGroup_coordinates_0_y').val())

        $('#rect-x').val(x)
        $('#rect-y').val(y)

        $('#rect-width').val((parseInt($('#tacticGroup_coordinates_1_x').val()) - x))
        $('#rect-height').val((parseInt($('#tacticGroup_coordinates_3_y').val()) - y))

        // Add the on change listener after initial setup
        $(".coordinate-rect").on('change', function(){
            drawCoordinates()
        });
    }

    // Initialize everything
    let numberRegex = /(\d+)/g;
    let template = $('.tactic-form').clone()
    let mapFloorSelectElement = $("#tacticGroup_mapFloorId")

    // Create map
    const map = L.map('mapDemo', {
        minZoom: 1,
        maxZoom: 3,
        zoomSnap: 0.25,
        crs: flippedYAxisCRS
    });

    // Place Zoom Control at screen bottom right of the screen
    map.zoomControl.setPosition('bottomright');
    map.off()

    let imageLayer = null;
    let myLayer = null;

    bindRemoveFormEvents()
    setInitialRectFromCoordinates()
    triggerMapFloorChange()

    mapFloorSelectElement.on('change', function(){
        triggerMapFloorChange()
    });

    map.addEventListener('mousemove', function(ev) {
        let x = parseInt(ev.latlng.lng);
        let y = parseInt(ev.latlng.lat);

        $('#mouse-coordinates').text("x: " + x + ", y: " + y)
    });
})