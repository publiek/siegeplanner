import json
import logging
import os
import pprint
import sys
import time
import tracemalloc
import re

import cv2
import pytesseract

from analyzers.operator_analyzer import OperatorAnalyzer
from analyzers.picking_phase_analyzer import PickingPhaseAnalyzer
from analyzers.round_analyzer import RoundAnalyzer
from analyzers.scoreboard_analyzer import ScoreboardAnalyzer
from helpers import crop
from models import AnalyzeResult, AnalyzeState

pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
# pytesseract.pytesseract.tesseract_cmd = '/usr/bin/tesseract/tesseract.exe'


class MatchStateAnalyzer:
    """Analysis screenshots and keeps state between screenshots to create match/round summaries"""

    def __init__(self, drawing, memory_snapshot):
        if memory_snapshot:
            tracemalloc.start()

        logging.basicConfig(stream=sys.stdout, level=logging.INFO)

        self.round_analyzer = RoundAnalyzer(drawing)
        self.operator_analyzer = OperatorAnalyzer(drawing, memory_snapshot)
        self.picking_phase_analyzer = PickingPhaseAnalyzer(drawing)
        self.current_map_text = 'unknown'
        self.current_bomb_site = 'unknown'
        self.rounds_processed = []
        self.files_processed = 0
        self.start_time = time.time()
        self.video_id = 'video_kKmqafv1jvk'

    def parse_player_data(self, player_data):
        filtered_players = []
        index = 0
        for player in player_data:
            index += 1
            if player['operator'] == 'unknown' or player['player_name'] == 'unknown':
                logging.info('incomplete player name: [%s], operator [%s], for index [%d]' % (player['player_name'], player['operator'], index))
                continue
            else:
                operator = re.sub("_\d+|\.png", "", player['operator'])
                filtered_players.append({'player': player['player_name'], 'operator': operator, 'index': index})

        return filtered_players

    def write_json(self, prefix, dict_object):
        data = json.dumps(dict_object)
        file_name = "../%s_results.json" % prefix
        f = open(file_name, "a")
        f.writelines(data + '\n')
        f.close()

        return file_name

    def mark_file(self, prefix, file_name, reason): os.rename(prefix + '/' + file_name, prefix + '/' + reason + file_name)

    def is_not_marked(self, file_name):
        for mark in AnalyzeState:
            if mark.name in file_name:
                return False

        return True

    def analyze_screenshot(self, video, screenshot, screenshot_name):
        start_time = time.time()
        picking_phase = self.picking_phase_analyzer.analyze(screenshot)
        if picking_phase and picking_phase[1] is not None:
            self.current_map_text, self.current_bomb_site = picking_phase
            logging.info("found picking phase, extracted map [%s] and bomb site [%s]" % (self.current_map_text, self.current_bomb_site))

            return AnalyzeResult(AnalyzeState.FOUND_PICKING_PHASE, True, time.time() - start_time)

        round_number = self.round_analyzer.analyze(screenshot)
        if round_number is None:
            logging.info("could not find round number for screenshot %s" % screenshot_name)

            return AnalyzeResult(AnalyzeState.NO_ROUND, True, time.time() - start_time)

        # prefix with map
        round_key = "%s_%d" % (self.current_map_text, round_number)

        if round_key not in self.rounds_processed:
            blue_operators = self.get_team_blue_operators(screenshot, self.operator_analyzer)
            if len(blue_operators) < 5:
                logging.warning(
                    "could not find enough operator data for blue team, skipping this screenshot, up to the next...")
                return AnalyzeResult(AnalyzeState.NO_OPS, True, time.time() - start_time)

            orange_operators = self.get_team_orange_operators(screenshot, self.operator_analyzer)
            if len(orange_operators) < 5:
                logging.warning(
                    "could not find enough operator data for orange team, skipping this screenshot, up to the next...")
                return AnalyzeResult(AnalyzeState.NO_OPS, True, time.time() - start_time)

            round_data = {
                'screenshot_name': screenshot_name,
                'round_number': round_number,
                'map': self.current_map_text,
                'bomb_site': self.current_bomb_site,
                'team_blue': blue_operators,
                'team_orange': orange_operators
            }

            result_file = self.write_json(video, round_data)
            logging.info("written operator data to %s for round %d on map %s" % (result_file, round_number, self.current_map_text))

            self.rounds_processed.append(round_key)
            return AnalyzeResult(AnalyzeState.FOUND_OPS, True, time.time() - start_time)
        else:
            logging.info("already processed round %d skipping" % round_number)
            return AnalyzeResult(AnalyzeState.ALREADY_PROCESSED, True, time.time() - start_time)

    def analyze_files(self, video):
        screenshot_files = filter(self.is_not_marked, os.listdir(video))

        for screenshot_file in sorted(screenshot_files, key=lambda x: int(x.split('.')[0])):
            logging.info("processing screenshot %s" % screenshot_file)

            if self.files_processed != 0 and self.files_processed % 20 == 0:
                logging.info("processing %.2f files/minute" % float(self.files_processed / ((time.time() - self.start_time) / 60)))

            self.files_processed += 1
            full_path = video + '/' + screenshot_file
            screenshot = cv2.imread(full_path)

            result = self.analyze_screenshot(video, screenshot, full_path)

            if result.success:
                self.mark_file(video, screenshot_file, result.state.name + "_")
            else:
                logging.error(result.error)

        logging.info("no more screenshot files found, processed the following rounds %s" % self.rounds_processed)

        time_elapsed = time.time() - self.start_time
        time_elapsed_formatted = time.strftime("%H:%M:%S", time.gmtime(time_elapsed))
        logging.info("took %s to process %d files  (%2.f files/minute)" % (time_elapsed_formatted, self.files_processed, self.files_processed / (time_elapsed / 60)))

    def get_team_blue_operators(self, team_screenshot, analyzer):
        logging.info("processing team blue")

        # first try small boxes
        team_blue = crop(team_screenshot, 40, 580, 196, 536)
        players = self.parse_player_data(analyzer.analyze(team_blue, False))

        # then big boxes
        if len(players) < 5:
            logging.info("trying big player boxes for team blue")
            team_blue = crop(team_screenshot, 40, 257, 425, 975)
            return self.parse_player_data(analyzer.analyze(team_blue, False, False))

        return players

    def get_team_orange_operators(self, team_screenshot, analyzer):
        logging.info("processing team orange")

        # first try small boxes
        team_orange = crop(team_screenshot, 2330, 580, 190, 536)
        players = self.parse_player_data(analyzer.analyze(team_orange, True))

        # then big boxes
        if len(players) < 5:
            logging.info("trying big player boxes for team orange")
            team_orange = crop(team_screenshot, 2106, 257, 425, 975)
            return self.parse_player_data(analyzer.analyze(team_orange, True, False))

        return players

