import logging
import re
import cv2
import numpy as np


class ScoreboardAnalyzer:
    """Checks which score/operator board is active"""

    def __init__(self, drawing):
        self.drawing = drawing
        # the white color threshold
        self.lower_threshold = 200
        self.upper_threshold = 255

    def analyze(self, screenshot):
        hsv = cv2.cvtColor(screenshot, cv2.COLOR_BGR2HSV)
        blue_lower = np.array([75, 75, 0], np.uint8)
        blue_upper = np.array([255, 255, 255], np.uint8)

        mask = cv2.inRange(hsv, blue_lower, blue_upper)
        ret, thresh = cv2.threshold(mask, 127, 255, 0)

        contours, hie = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        player_boxes = []
        for i in range(5):
            # Find the index of the largest contour
            areas = [cv2.contourArea(c) for c in contours]
            max_index = np.argmax(areas)
            cnt = contours[max_index]
            rect = cv2.boundingRect(cnt)
            x, y, w, h = rect
            cv2.rectangle(screenshot, (x, y), (x + w, y + h), (100, 255, 100), 5)
            # cv2.drawContours(screenshot, cnt, -1, (0, 255, 0), 3, cv2.FILLED)
            player_boxes.append(rect)
            del contours[max_index]

        cv2.imshow('scoreboard_blue.png', screenshot)
        cv2.waitKey(3000)
