import logging
import os
import time
import tracemalloc

import cv2
import numpy as np

from analyzers.player_name_analyzer import PlayerNameAnalyzer
from helpers import mem_top
from math_functions import find_box, get_clusters_by_mean_shift, get_nearest_neighbours, get_translation_matrix, \
    get_translated_points
from models import IconMatch, PlayerCardBox


class OperatorAnalyzer:
    """Searches and assigns which player picked what operator"""

    def __init__(self, drawing, memory_snapshot):
        # to show match result drawings
        self.drawing = drawing
        # tracemalloc memory debugging
        self.memory_snapshot = memory_snapshot
        # find multiple occurrences through clustering
        self.find_multiple = False

        self.icons_path = os.getcwd() + "\\icons"
        self.icon_img_resize_factor = 2
        self.icon_point_resize_factor = 0.5
        self.team_img_resize_factor = 4

        # players in scoreboard
        self.player_cards = 5

        self.small_boxes = self.create_player_boxes(195, 108)
        self.big_boxes = self.create_player_boxes(412, 192)
        self.active_boxes = self.small_boxes

        # the distance between points, trade off between match count and accuracy
        self.nearest_neighbours_match_ratio = 0.7
        # minimal points to match
        self.minimal_matched_points = 2
        self.player_name_analyzer = PlayerNameAnalyzer(drawing)

        # AKAZE detector
        self.detector = cv2.AKAZE_create()

        self.icon_file_list = os.listdir(self.icons_path)
        self.icon_list = []

        for icon_file in self.icon_file_list:
            icon_img = cv2.imread(self.icons_path + '/' + icon_file)
            icon_img = cv2.resize(icon_img, None, fx=self.icon_img_resize_factor, fy=self.icon_img_resize_factor, interpolation=cv2.INTER_CUBIC)
            kps1, desc1 = self.detector.detectAndCompute(icon_img, None)

            self.icon_list.append((icon_img, icon_file, kps1, desc1))

    def wait_for_icon_list(self):
        while len(self.icon_file_list) != len(self.icon_list):
            logging.info("waiting to icons have been initialized...")
            time.sleep(1000)

    def analyze(self, team_image, team_orange=False, try_small=True):
        if try_small:
            self.active_boxes = self.small_boxes
        else:
            self.active_boxes = self.big_boxes

        # raw image for player name analyzing
        team_raw_image = team_image.copy()

        # increase player team size
        team_image = cv2.resize(team_image, None, fx=self.team_img_resize_factor, fy=self.team_img_resize_factor, interpolation=cv2.INTER_CUBIC)
        # calculate key points and descriptors for team image
        kps2, desc2 = self.detector.detectAndCompute(team_image, None)

        self.wait_for_icon_list()

        player_data = []
        for i in range(self.player_cards):
            player_data.insert(i, {'player_name': 'unknown', 'operator': 'unknown', 'score': 0})

        for icon_img, icon_file, kps1, desc1 in self.icon_list:
            icon_results = self.find_icon_occurrences(icon_img, team_image, kps1, desc1, kps2, desc2, icon_file)

            for icon_result in icon_results:
                logging.debug("found icon %s assigning to player box" % icon_file)

                box = self.fit_icon_in_player_card(self.active_boxes, icon_result)
                if box is not None and box.player_number is not None and box.player_number >= 0:
                    logging.info("index [%d], found player icon [%s] with score [%d]" % (box.player_number, icon_file, icon_result.match_count))

                    # choose the highest score operator
                    current = player_data[box.player_number]
                    if current['score'] < icon_result.match_count:
                        logging.info("index [%d], choosing player icon [%s] has highest score [%d]" % (box.player_number, icon_file, icon_result.match_count))
                        name = self.player_name_analyzer.analyze(team_raw_image, box, team_orange)
                        player_data[box.player_number] = {'player_name': name, 'operator': icon_file,
                                                          'score': icon_result.match_count}

        return player_data

    def get_icon_coordinates(self, desc1, desc2, kps1, kps2, icon_img, team_cv_image):
        if desc2 is None or kps2 is None:
            logging.warning("Target image has invalid descriptors: [%s] or keypoints: [%s]" % (desc2, kps2))
            return None

        matched1, matched2 = get_nearest_neighbours(desc1, desc2, kps1, kps2, self.nearest_neighbours_match_ratio)

        logging.debug("Found %d matching points" % len(matched1))

        if len(matched1) < self.minimal_matched_points or len(matched2) < self.minimal_matched_points:
            logging.debug("Not enough matches found in target image")
            return None

        matrix = get_translation_matrix(matched1, matched2)

        if matrix is None:
            logging.debug("Could not create translation matrix")
            return None

        # icon width
        height = icon_img.shape[0]
        width = icon_img.shape[1]

        p0, p1, p2, p3 = get_translated_points(matrix, width, height)

        if self.drawing:
            for box in self.active_boxes:
                team_cv_image = cv2.rectangle(team_cv_image, box.top_left, box.bottom_right, (255, 0, 0), 8)

            cv2.line(team_cv_image, p0, p1, (0, 255, 0), 8)
            cv2.line(team_cv_image, p1, p2, (0, 255, 0), 8)
            cv2.line(team_cv_image, p2, p3, (0, 255, 0), 8)
            cv2.line(team_cv_image, p3, p0, (0, 255, 0), 8)

            team_cv_image = cv2.resize(
                team_cv_image,
                None,
                fx=1 / self.team_img_resize_factor,
                fy=1 / self.team_img_resize_factor,
                interpolation=cv2.INTER_CUBIC
            )

            cv2.imshow("team_operators.png", team_cv_image)
            cv2.waitKey(1000)

        if float(len(matched1)) == 0:
            logging.info("%s cannot divide by zero" % len(matched1))
            return None

        # resize the icon positions back to what they were before resizing so they are relative to the player boxes
        factor = self.icon_point_resize_factor
        return IconMatch(
            (max(0, (p0[0] - (width * factor))), max(0, (p0[1] - (height * factor)))),
            (max(0, (p1[0] - (width * factor))), max(0, (p1[1] - (height * factor)))),
            (max(0, (p2[0] - (width * factor))), max(0, (p2[1] - (height * factor)))),
            (max(0, (p3[0] - (width * factor))), max(0, (p3[1] - (height * factor)))),
            len(matched1)
        )

    def find_icon_occurrences(self, icon_img, team_cv_image, kps1, desc1, kps2, desc2, name=''):
        logging.debug("trying to find %s" % name)
        # greyscale test
        # icon = cv2.cvtColor(icon, cv2.COLOR_BGR2GRAY)
        # team_cv_image = cv2.cvtColor(team_cv_image, cv2.COLOR_BGR2GRAY)

        # Detect points and get descriptors
        # kps1, desc1 = detector.detectAndCompute(icon, None)
        # kps2, desc2 = self.detector.detectAndCompute(team_cv_image, None)

        if self.memory_snapshot:
            mem_top(tracemalloc.take_snapshot())

        logging.debug("template keypoints : %s" % len(kps1))
        logging.debug("screenshot keypoints : %s" % len(kps2))

        if self.find_multiple:
            mean_shift, cluster_sets, labels = get_clusters_by_mean_shift(kps2)
            desc2_ = desc2

            match_results = []
            for i in range(len(cluster_sets)):
                # Key points from cluster
                kps2 = cluster_sets[i]
                label = mean_shift.labels_
                dots, = np.where(label == i)
                # Descriptors from cluster
                desc2 = desc2_[dots, ]

            rect = self.get_icon_coordinates(desc1, desc2, kps1, kps2, icon_img, team_cv_image)
            if rect is not None:
                match_results.append(rect)

            return match_results
        else:
            rect = self.get_icon_coordinates(desc1, desc2, kps1, kps2, icon_img, team_cv_image)
            if rect is not None:
                return [rect]
            else:
                return []

    def fit_icon_in_player_card(self, player_card_boxes, icon_result):
        for box in player_card_boxes:
            if find_box(box.top_left, box.bottom_right, icon_result):
                return box

        return None

    def create_player_boxes(self, width, height):
        original_width = width
        original_height = height
        width = width * self.team_img_resize_factor
        height = height * self.team_img_resize_factor
        player_card_boxes = []
        y = 0
        oy = 0
        # Create 5 boxes for each operator card
        for i in range(self.player_cards):
            p1 = (0, y)
            op1 = (0, oy)
            y += height
            oy += original_height
            p2 = (width, y)
            op2 = (original_width, oy)

            player_card_boxes.append(PlayerCardBox(
                player_number=i,
                top_left=p1,
                bottom_right=p2,
                original_top_left=op1,
                original_bottom_right=op2
            ))

        return player_card_boxes
