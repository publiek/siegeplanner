import logging

import cv2

from helpers import crop, extract_text


class PlayerNameAnalyzer:
    """Searches and extracts the player name within the player box area"""

    def __init__(self, drawing):
        self.drawing = drawing
        self.text_color_threshold = 210
        self.player_name_width = 200
        self.player_name_height = 45

    def analyze(self, team_image, box, from_top_left=False):
        (x, y) = box.original_top_left

        # prevents cropping outside the image space, which results in faulty images / analyze result
        if self.player_name_width > box.o_width():
            player_name_width = box.o_width()
        else:
            player_name_width = self.player_name_width

        if from_top_left:
            player_name_image = crop(team_image, x, y, player_name_width, self.player_name_height)
        else:
            (x, _) = box.original_bottom_right
            player_name_image = crop(team_image, x - player_name_width, y, self.player_name_width, self.player_name_height)

        # Page segmentation mode 7 (Handle as a single line of text)
        player_score_text = extract_text(player_name_image, self.text_color_threshold, '--psm 7', self.drawing, 'player.png')

        logging.info("index [%d], found player name [%s]" % (box.player_number, player_score_text))
        return player_score_text
