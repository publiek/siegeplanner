import logging

from helpers import crop, extract_text


class PickingPhaseAnalyzer:
    """Searches and extracts map name and bomb site on a picking phase screenshot"""

    def __init__(self, drawing):
        self.drawing = drawing
        self.text_color_threshold = 205
        self.map_list = ["bank", "border", "chalet", "clubhouse", "coastline", "consulate", "favela", "fortress", "hereford base", "house", "kafe dostoyevsky", "kanel", "oregon", "outback", "presidential plane", "skyscraper", "theme park", "tower", "villa", "yacht"]
        self.character_black_list = ['-', '~', ',', '_']

    def analyze(self, screenshot):
        map_name_image = crop(screenshot, 151, 69, 544, 64)
        # page segmentation mode 7 (Handle as a single line of text)
        map_text = extract_text(map_name_image, self.text_color_threshold, '--psm 7', self.drawing, 'map.png')

        if map_text is not None:
            map_text = map_text.lower().strip()

            if map_text in self.map_list:
                bomb_site_blue = self.filter_bomb_site(self.get_bomb_site(screenshot, False))

                if bomb_site_blue is not None:
                    return map_text, bomb_site_blue
                else:
                    return map_text, self.filter_bomb_site(self.get_bomb_site(screenshot, True))

    def get_bomb_site(self, screenshot, team_orange):
        if team_orange:
            bomb_site_image = crop(screenshot, 1080, 1235, 420, 81)
            return extract_text(bomb_site_image, 215, None, self.drawing, 'bomb.png')
        else:
            bomb_site_image = crop(screenshot, 1080, 746, 420, 81)
            return extract_text(bomb_site_image, 225, None, self.drawing, 'bomb.png')

    def filter_bomb_site(self, text):
        if text is not None:
            # TODO: validate bomb sites like we do for maps
            for b in self.character_black_list:
                text = text.replace(b, "")

            # TODO: for now a cheat, with a new line is the bomb site
            if len(text.split("\n")) > 1:
                return text.lower().strip().replace('\n', ',')
