import logging
import re
import cv2
from helpers import crop, extract_text


class RoundAnalyzer:
    """Searches and extract the current round number"""

    def __init__(self, drawing):
        self.drawing = drawing
        # the white color threshold
        self.threshold = 180
        self.round_score_threshold = 150

    def analyze(self, screenshot):
        # round image location
        image = crop(screenshot, 1180, 121, 197, 36)

        # page segmentation mode 7 (Handle as a single line of text)
        round_text = extract_text(image, self.threshold, '--psm 7', self.drawing, 'round.png')
        clean_text = round_text.lower().strip()

        # end of match check
        if 'match' in clean_text or 'point' in clean_text or 'overtime':
            blue_score_image = crop(screenshot, 1229, 68, 48, 48)
            orange_score_image = crop(screenshot, 1283, 68, 48, 48)

            try:
                blue_score = int(extract_text(blue_score_image, self.round_score_threshold, '--psm 7', self.drawing, 'round_blue.png', cv2.THRESH_BINARY))
                orange_score = int(extract_text(orange_score_image, self.round_score_threshold, '--psm 7', self.drawing, 'round_orange.png', cv2.THRESH_BINARY))
            except ValueError:
                logging.warning("Could not extract score data")
                return None

            predicted_round = blue_score + orange_score + 1
            logging.info("Match point, calculate round score : [%d]", predicted_round)
            return predicted_round

        # extract the roud number only
        pattern = "^[a-zA-Z]+ (\d+)$"
        match = re.match(pattern, round_text.lower().strip())

        if match:
            extracted_round_number = int(match.group(1))
            logging.info("extracted round : [%d]", extracted_round_number)
            return extracted_round_number

        logging.warning("Could not extract round data, found text : [%s]", round_text)
