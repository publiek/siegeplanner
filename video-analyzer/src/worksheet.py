import json
import logging
import os
import pprint
import sys
import time
import tracemalloc
from helpers import crop
from analyzers.match_state_analyzer import MatchStateAnalyzer
from analyzers.operator_analyzer import OperatorAnalyzer
from analyzers.picking_phase_analyzer import PickingPhaseAnalyzer
from analyzers.round_analyzer import RoundAnalyzer
from analyzers.scoreboard_analyzer import ScoreboardAnalyzer
import cv2

# pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
# pytesseract.pytesseract.tesseract_cmd = '/usr/bin/tesseract/tesseract.exe'

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

drawing_enabled = False
memory_snapshot_enabled = False
find_multiple = False

video_id = 'video_h0QefbKJCcY'

# analyzer = MatchStateAnalyzer(drawing_enabled, memory_snapshot_enabled)
# analyzer.analyze_files(video_id)

# folded_team = crop(cv2.imread('screenshots/open_and_folded_scoreboard.png'), 0, 257, 650, 1275)
# ScoreboardAnalyzer(True).analyze(folded_team)

# screenshot1 = cv2.imread('video_kKmqafv1jvk/processed_pp_43.png')
# logging.info(PickingPhaseAnalyzer(True).analyze(screenshot1))
# screenshot2 = cv2.imread('screenshots/picking_phase_switched.png')
# logging.info(PickingPhaseAnalyzer(True).analyze(screenshot2))
#
debug_file = 'screenshots/highres_screenshot.png'
# blue team
team_blue = crop(cv2.imread(debug_file), 40, 257, 425, 975)
operators = OperatorAnalyzer(True, False).analyze(team_blue, False, False)
pprint.PrettyPrinter().pprint(operators)

# orange team
team_orange = crop(cv2.imread(debug_file), 2106, 257, 425, 975)
operators = OperatorAnalyzer(True, False).analyze(team_orange, True, False)
pprint.PrettyPrinter().pprint(operators)

# small team analyze
# team_blue = crop(cv2.imread('screenshots/folded_scoreboard.png'), 40, 582, 196, 536)
# players = OperatorAnalyzer(False, False).analyze(team_blue, False, True)
#
# # small team analyze
# team_orange = crop(cv2.imread('screenshots/folded_scoreboard.png'), 2330, 582, 190, 536)
# players = OperatorAnalyzer(False, False).analyze(team_orange, False, True)

# exit(0)
