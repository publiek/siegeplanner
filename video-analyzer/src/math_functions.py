import logging
import numpy as np
from sklearn.cluster import MeanShift, estimate_bandwidth
import cv2


def find_point(x1, y1, x2, y2, point_x, point_y):
    if point_x >= x1 and point_x <= x2 and point_y >= y1 and point_y <= y2:
        logging.debug('point match %s %s' % (point_x, point_y))
        return True

    return False


def find_box(p1, p2, icon):
    if (find_point(*p1, *p2, *icon.top_left) and
            find_point(*p1, *p2, *icon.top_right) and
            find_point(*p1, *p2, *icon.bottom_left) and
            find_point(*p1, *p2, *icon.bottom_right)):
        return True

    return False


def get_clusters_by_mean_shift(target_key_points):
    x = np.array([target_key_points[0].pt])

    for i in range(len(target_key_points)):
        x = np.append(x, [target_key_points[i].pt], axis=0)

    x = x[1:len(x)]

    bandwidth = estimate_bandwidth(x, quantile=0.1, n_samples=500)

    # mean shift cluster the keypoints in the target
    ms = MeanShift(bandwidth=bandwidth, bin_seeding=True, cluster_all=True)
    ms.fit(x)
    labels = ms.labels_

    labels_unique = np.unique(labels)
    n_clusters_ = len(labels_unique)
    logging.debug("found %d cluster occurences" % n_clusters_)

    cluster_sets = [None] * n_clusters_
    for i in range(n_clusters_):
        label = ms.labels_
        dots, = np.where(label == i)
        logging.debug("number of dots in cluster %d" % len(dots))
        cluster_sets[i] = list(target_key_points[dot] for dot in dots)

    return ms, cluster_sets, labels


def get_nearest_neighbours(desc1, desc2, kps1, kps2, nn_match_ratio):
    # We use Hamming distance, because AKAZE uses binary descriptor by default
    matcher = cv2.DescriptorMatcher_create(cv2.DescriptorMatcher_BRUTEFORCE_HAMMING)
    # Get the nearest neighbour descriptor points
    nn_matches = matcher.knnMatch(desc1, desc2, 2)

    matched1 = []
    matched2 = []

    for m, n in nn_matches:
        if m.distance < nn_match_ratio * n.distance:
            matched1.append(kps1[m.queryIdx])
            matched2.append(kps2[m.trainIdx])

    logging.debug("Found %s nearest neighbours" % len(nn_matches))

    return matched1, matched2


def get_translation_matrix(matched1, matched2):
    # Create a matrix to translate between src and destination points
    src_pts = np.float32([m.pt for m in matched1]).reshape(-1, 1, 2)
    dst_pts = np.float32([m.pt for m in matched2]).reshape(-1, 1, 2)

    matrix, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)

    return matrix


def get_translated_points(matrix, width, height):
    # create bounding box
    bounding_box = ((0, 0), (width, 0), (width, height), (0, height))

    # Linear algebra transform original bounding box to target position.
    # (x, y) -> (x_r, y_r)
    #
    # [       ]   [x]   [x_r / l]
    # [   T   ] . [y] = [y_r / l]
    # [       ]   [1]   [   l   ]
    #
    points = np.asarray([m + (1,) for m in bounding_box]).transpose()
    points_t = np.dot(matrix, points)

    x_points_t = points_t[0] / points_t[2]
    y_points_t = points_t[1] / points_t[2]

    # returns 4 positions p0, p1, p2, p3
    return tuple(zip(tuple([int(x) for x in x_points_t]), tuple([int(y) for y in y_points_t])))
