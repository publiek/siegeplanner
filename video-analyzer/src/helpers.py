import linecache
import os
import tracemalloc
import logging
import cv2
import pytesseract
import numpy

def mem_top(snapshot, key_type='lineno', limit=3):
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)

    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        # replace "/path/to/module/file.py" with "module/file.py"
        filename = os.sep.join(frame.filename.split(os.sep)[-2:])
        logging.debug("#%s: %s:%s: %.1f KiB" % (index, filename, frame.lineno, stat.size / 1024))
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            logging.debug('    %s' % line)

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        logging.debug("%s other: %.1f KiB" % (len(other), size / 1024))
    total = sum(stat.size for stat in top_stats)
    logging.debug("Total allocated size: %.1f KiB" % (total / 1024))


def crop(cv_img, x, y, width, height): return cv_img[y:(y + height), x:(x + width)]


def extract_text(screenshot, threshold, config=None, drawing=False, unique_name='text.png', binary=cv2.THRESH_BINARY_INV):
    # zoom in
    image = cv2.resize(screenshot, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
    # greyscale
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # threshold the colors
    x, image = cv2.threshold(image, threshold, 255, binary)
    # make it more smooth for tesseract
    image = cv2.GaussianBlur(image, (0, 0), 0.5)

    if drawing:
        cv2.imshow(unique_name, image)
        cv2.waitKey(1000)

    if config:
        return pytesseract.image_to_string(image, config=config)
    else:
        return pytesseract.image_to_string(image)
