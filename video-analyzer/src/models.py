from dataclasses import dataclass
from enum import Enum


@dataclass()
class IconMatch:
    top_left: (int, int)
    top_right: (int, int)
    bottom_left: (int, int)
    bottom_right: (int, int)
    match_count: int


@dataclass()
class PlayerCardBox:
    player_number: int
    top_left: (int, int)
    bottom_right: (int, int)
    original_top_left: (int, int)
    original_bottom_right: (int, int)

    def o_width(self): return self.original_bottom_right[0] - self.original_top_left[0]
    def o_height(self): return self.original_bottom_right[1] - self.original_top_left[1]


class AnalyzeState(Enum):
    FOUND_PICKING_PHASE = 1
    FOUND_OPS = 2
    NO_ROUND = 3
    NO_OPS = 4
    ALREADY_PROCESSED = 5


@dataclass()
class AnalyzeResult:
    state: AnalyzeState
    success: bool
    duration_sec: int = 0
    error: Exception = None

