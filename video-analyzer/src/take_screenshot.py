from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from PIL import Image
from io import BytesIO
import time
import logging
import os
import sys
import threading
import urllib.parse as urlparse
from urllib.parse import parse_qs
from analyzers.match_state_analyzer import MatchStateAnalyzer
import cv2
import numpy

def get_video_key(url):
    parsed = urlparse.urlparse(url)
    return parse_qs(parsed.query)['v'].pop()


logging.basicConfig(stream=sys.stdout, level=logging.INFO)

# url = "https://www.youtube.com/watch?v=h0QefbKJCcY&vq=hd1080&t=863"
url = "https://www.youtube.com/watch?v=kKmqafv1jvk&vq=hd1080&t=760"
PLAYER_ID = "ytd-player"
PLAYER_CINEMA_MODE = "ytp-size-button"
PLAYER_FULLSCREEN_MODE = "ytp-fullscreen-button"
PLAYER_CURRENT_TIME = "ytp-time-current"

screenshot_interval = 5.0

# Driver settings
driver_options = Options()
driver_options.add_argument("--mute-audio")
driver_options.add_argument("--incognito")
# driver_options.add_argument("--headless")
# https://chromedriver.chromium.org/ chrome needs to be the same version
driver = webdriver.Chrome(executable_path="C:\\Users\\Tim\\chromedriver", options=driver_options)
# driver = webdriver.Chrome(executable_path="/usr/bin/chromedriver", chrome_options=driver_options)
driver.set_window_position(-2500, 0)
driver.set_window_size(2040, 1308)
driver.get(url)

logging.info("Waiting for page to load")
time.sleep(5)

driver.find_element_by_class_name(PLAYER_FULLSCREEN_MODE).click()
logging.info("Open cinema mode & start video")
time.sleep(1)

# player = driver.find_element_by_id(PLAYER_ID)
# player.click()
# logging.info("Starting video")
# time.sleep(1)

# removes survey popup
logging.info("Removing survey popup")
driver.execute_script("list = document.getElementsByName('paper-dialog'); if (list.length > 0) list.item(0).remove();")
time.sleep(1)


def start_screenshot_files():
    screenshot_path = 'video_%s' % get_video_key(url)
    os.mkdir(screenshot_path)

    def take_screenshot_file(count):
        screenshot = driver.find_element_by_id(PLAYER_ID).screenshot_as_png
        image = Image.open(BytesIO(screenshot))
        image = image.convert("RGB")
        filename = "%s/%d.png" % (screenshot_path, count)
        image.save(filename)
        logging.info("Saved screenshot to %s" % filename)
        threading.Timer(screenshot_interval, take_screenshot_file, [count + 1.0]).start()

    take_screenshot_file(0)

    logging.info("Taking screenshots every %d seconds" % screenshot_interval)


def start_screenshot_in_memory():
    drawing_enabled = False
    memory_snapshot_enabled = False
    analyzer = MatchStateAnalyzer(drawing_enabled, memory_snapshot_enabled)
    video = get_video_key(url)

    def get_video_time():
        return driver.execute_script("ytplayer = document.getElementById('movie_player'); return ytplayer.getCurrentTime();")

    def take_screenshot_analyze_immediately(count):
        encoded_data = driver.find_element_by_id(PLAYER_ID).screenshot_as_png
        image = Image.open(BytesIO(encoded_data))

        video_timestamp = str(int(get_video_time()))
        result = analyzer.analyze_screenshot(video, numpy.array(image), "time_" + video_timestamp)

        logging.info("Analyzed timestamp [%s] and determined [%s] in [%d] seconds" % (video_timestamp, result.state.name, result.duration_sec))
        threading.Timer(screenshot_interval, take_screenshot_analyze_immediately, [count + 1.0]).start()

    take_screenshot_analyze_immediately(0)

    logging.info("Taking screenshots every %d seconds" % screenshot_interval)


start_screenshot_in_memory()