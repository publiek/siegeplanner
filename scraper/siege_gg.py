#!/usr/bin/env python
# -*- coding: utf-8 -*-

import scrapy
import re
import logging
from scrapy.crawler import CrawlerProcess

class OperatorBan(scrapy.Item):
    operator = scrapy.Field()
    map = scrapy.Field()
    banned_by_team = scrapy.Field()
    default_bans = scrapy.Field()
    match_id = scrapy.Field()
    match_date = scrapy.Field()
    season = scrapy.Field()
    season_number = scrapy.Field()
    region = scrapy.Field()


class SiegeGGScraper(scrapy.Spider):
    name = 'bingbot'
    operator_ban_counter = 0
    start_urls = []
    for page in range(68):
        start_urls.append('https://siege.gg/matches?page=' + str(page))

    operators = ["recruit", "sledge", "thatcher", "ash", "thermite", "twitch", "montagne", "glaz", "fuze", "blitz", "iq", "buck", "blackbeard", "capitão", "capitao", "hibana", "jackal", "ying", "zofia", "dokkaebi", "lion", "finka", "maverick", "nomad", "gridlock", "nøkk", "nokk", "amaru", "kali", "smoke", "mute", "castle", "pulse", "doc", "rook", "kapkan", "tachanka", "jäger", "jager", "bandit", "frost", "valkyrie", "caveira", "echo", "mira", "lesion", "ela", "vigil", "alibi", "maestro", "clash", "kaid", "mozzie", "warden", "goyo", "wamai"]
    maps = ["bank", "border", "chalet", "clubhouse", "coastline", "consulate", "favela", "fortress", "hereford-base", "house", "kafe-dostoyevsky", "kanel", "oregon", "outback", "presidential-plane", "skyscraper", "theme-park", "tower", "villa", "yacht"]

    def validate_operator(self, op):
        if op and op.lower().strip() in self.operators:
            return op.lower().strip()

        return None

    def validate_map(self, map):
        if map and map.lower().strip() in self.maps:
            return map.lower().strip()

        return None

    def validate_meta(self, meta):
        if meta:
            return meta.lower().strip()

        return ""

    def extract_bans(self, response):
        logging.info("Visiting match result page : " + response.url)
        matched_match_id = re.search("/matches/(\d+)", response.url)
        if not matched_match_id:
            logging.warning("Could not find match id for uniqueness skipping page : " + response.url)
            return

        match_id = matched_match_id.group(1)
        match_played_date = response.css('.impact__title > time').xpath('@datetime').get()
        default_bans = response.css('.ban__ops .alert .ban__subject::text').getall()
        season = self.validate_meta(response.css('.entry__meta > .meta__season::text').get())
        season_number = self.validate_meta(response.css('.entry__meta > .match__season-number::text').get())
        region = self.validate_meta(response.css('.entry__meta > .match__region::text').get())

        for maps in response.css('.ban__ops > .tab-content > .tab-pane'):
            matched_map = re.search("^op-ban-content-(.*)$", maps.xpath('@id').get())
            if not matched_map:
                continue

            map_name = matched_map.group(1)
            if map_name != 'default':
                for ban in maps.css('.ban__ops__list > li > .ban__op__text'):
                    operator = self.validate_operator(ban.css('.ban__op__text > .ban__subject::text').get())
                    team = ban.css('.ban__op__text > .ban__team::text').get()

                    if not operator or not team:
                        continue

                    self.operator_ban_counter += 1

                    if (self.operator_ban_counter % 10 == 0):
                        logging.info("So far gathered " + str(self.operator_ban_counter) + " operator bans")

                    yield OperatorBan(
                        map=map_name,
                        operator=operator,
                        banned_by_team=team,
                        default_bans=default_bans,
                        match_id=match_id,
                        match_date=match_played_date,
                        season=season,
                        season_number=season_number,
                        region=region
                    )

    def parse(self, response):
        logging.info("Visiting index page : " + response.url)
        for match_result_page in response.css('.match--has-results').xpath('@href').getall():
            yield response.follow(match_result_page, self.extract_bans)


process = CrawlerProcess(settings={
    'FEED_FORMAT': 'jsonlines',
    'FEED_URI': 'results/siege_gg_bans.json',
    'LOG_LEVEL': 'INFO',
    'AUTOTHROTTLE_ENABLED': True,
    'DOWNLOAD_DELAY': 0.25
})
process.crawl(SiegeGGScraper)
process.start()
