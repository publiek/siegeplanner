## siegeplanner

### initial ban data (with https://scrapy.org/)

Initial ban data from siege.gg, careful not to scrape to many times (ddos)
 
```
$ pip3 install wheel
$ pip3 install scrapy
$ cd scrapers
$ python siege_gg.py
```

see scraper/results/siege_gg_bans.json