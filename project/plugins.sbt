// Flyway
addSbtPlugin("io.github.davidmweber" % "flyway-sbt" % "5.0.0")

libraryDependencies += "org.postgresql" % "postgresql" % "42.2.1"

// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.0")
