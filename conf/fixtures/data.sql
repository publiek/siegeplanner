insert into map ("name", slug) values
    ('Bank', 'bank'),
    ('Oregon', 'oregon'),
    ('Border', 'border'),
    ('Clubhouse', 'clubhouse')
;

insert into map_floor ("level", file, map_id) values
    (-1, 'floors/bank/basement.jpg', 1),
    (0, 'floors/bank/ground.jpg', 1),
    (1, 'floors/bank/top.jpg', 1)
;

insert into tactic_group (location_name, coordinates, map_floor_id, created_at, modified_at) values
    ('Bombsite hallway', ARRAY[[324,153],[330,153],[330,171],[324,171]], 1, now(), now()),
    ('Bombsite doorway access', ARRAY[[224,53],[230,53],[230,71],[224,71]], 2, now(), now())
;

insert into operator ("name", side, icon) values
	('Mira', 'defender', 'mira.svg'),
	('Jager', 'defender', 'jager.svg')
;

insert into tactic ("name", description, links, video_url, thumbnail_url, operator_id, tactic_group_id) values(
	'Mira window to protect bomb',
	'Open up the left wall and reinforce the right wall. Place a mira window on the reinforced wall. This setups a nice peek window which proctects entry to the bomb site.',
	ARRAY['https://www.ubisoft.com/en-gb/game/rainbow-six/siege/game-info/operators/mira'],
	'https://example.com',
	'https://steamuserimages-a.akamaihd.net/ugc/910171378792707645/5D40A5C601EA64F912212DB6FAC831145580203C/',
	1,
	1
);

insert into tactic ("name", description, links, video_url, thumbnail_url, operator_id, tactic_group_id) values(
	'Mira window to protect hallway',
	'Open up the left wall and reinforce the right wall. To peek the hallway',
	ARRAY['https://www.ubisoft.com/en-gb/game/rainbow-six/siege/game-info/operators/mira'],
	'https://example.com',
	'https://external-preview.redd.it/p4A_VwDnsukOjqslGTPWWsuQEq2dWze0beGJ8tiCJ8Q.png?format=pjpg&auto=webp&s=ce0243dadb3f621401d2a3ab8635c757b2ca1694',
	1,
	2
);