CREATE OR REPLACE FUNCTION trigger_insert()
  RETURNS TRIGGER AS $$
BEGIN
  NEW.created_at = now();
  NEW.modified_at = now();
  RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION trigger_update()
    RETURNS TRIGGER AS $$
BEGIN
    NEW.modified_at = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TABLE operator_ban (
  id SERIAL,
  json JSONB,
  created_at timestamp NULL DEFAULT NULL,
  modified_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TRIGGER operator_ban_created_at BEFORE INSERT
  on operator_ban FOR EACH ROW EXECUTE PROCEDURE
  trigger_insert();
