CREATE TABLE map (
  id SERIAL,
  name VARCHAR(1024),
  slug VARCHAR(1024),
  created_at timestamp NULL DEFAULT NULL,
  modified_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE map_floor (
  id SERIAL,
  level INTEGER,
  file VARCHAR(1024),
  map_id INTEGER REFERENCES map(id),
  PRIMARY KEY (id)
);

CREATE TRIGGER map_created_at BEFORE INSERT
  ON map FOR EACH ROW EXECUTE PROCEDURE
  trigger_insert();

CREATE TRIGGER map_modified_at BEFORE UPDATE
  ON map FOR EACH ROW EXECUTE PROCEDURE
  trigger_update();

CREATE TABLE operator (
  id SERIAL,
  name VARCHAR(1024),
  created_at timestamp NULL DEFAULT NULL,
  modified_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TRIGGER operator_created_at BEFORE INSERT
  ON operator FOR EACH ROW EXECUTE PROCEDURE
  trigger_insert();

CREATE TRIGGER operator_modified_at BEFORE UPDATE
  ON operator FOR EACH ROW EXECUTE PROCEDURE
  trigger_update();

CREATE TABLE tactic_group (
  id SERIAL,
  coordinates INTEGER[][],
  map_floor_id INTEGER REFERENCES map_floor(id),
  created_at TIMESTAMP NULL DEFAULT NULL,
  modified_at TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TRIGGER tactic_group_created_at BEFORE INSERT
  on tactic_group FOR EACH ROW EXECUTE PROCEDURE
  trigger_insert();

CREATE TRIGGER tactic_group_modified_at BEFORE UPDATE
  ON tactic_group FOR EACH ROW EXECUTE PROCEDURE
  trigger_update();

CREATE TABLE tactic (
  id SERIAL,
  name VARCHAR(1024),
  description TEXT NULL DEFAULT NULL,
  links VARCHAR(1024)[],
  video_url VARCHAR(1024) NULL DEFAULT NULL,
  thumbnail_url VARCHAR(1024) NULL DEFAULT NULL,
  created_at TIMESTAMP NULL DEFAULT NULL,
  modified_at TIMESTAMP NULL DEFAULT NULL,
  operator_id INTEGER REFERENCES operator(id),
  tactic_group_id INTEGER REFERENCES tactic_group(id),
  PRIMARY KEY (id)
);

CREATE TRIGGER tactic_created_at BEFORE INSERT
  on tactic FOR EACH ROW EXECUTE PROCEDURE
  trigger_insert();

CREATE TRIGGER tactic_modified_at BEFORE UPDATE
  ON tactic FOR EACH ROW EXECUTE PROCEDURE
  trigger_update();
