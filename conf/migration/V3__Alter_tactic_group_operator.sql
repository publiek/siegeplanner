
ALTER TABLE tactic_group ADD COLUMN location_name VARCHAR(1024);

CREATE TYPE side_enum AS ENUM ('attacker', 'defender');
ALTER TABLE operator ADD COLUMN side side_enum;
ALTER TABLE operator ADD COLUMN icon VARCHAR(1024);