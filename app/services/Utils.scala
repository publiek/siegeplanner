package services

import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import play.api.libs.json.{JsString, JsValue, Writes}

object Utils {

  /**
   * Converts Joda DateTime to json string with a given pattern i.e. yyyy-MM-dd HH:mm:ss
   */
  def jodaDateWrites(pattern: String): Writes[DateTime] = new Writes[DateTime] {
    val df: DateTimeFormatter = DateTimeFormat.forPattern(pattern)
    def writes(d: DateTime): JsValue = JsString(d.toString(df))
  }
}
