package services.providers

import javax.inject._
import models.repositories.{MapRepository, MapWithFloors, TacticGroup, TacticRepository}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class MapFloorProvider @Inject()(
    mapRepository: MapRepository,
    tacticRepository: TacticRepository
  )(implicit ec: ExecutionContext) {

  def fetchByMap(mapSlug: String): Future[(Option[MapWithFloors], Seq[TacticGroup])] = {
    for {
      map <- mapRepository.findBySlug(mapSlug)
      tacticGroups <- tacticRepository.findByMapFloor(
        map.map(m => m.floors.map(_.id).toSet)
          .getOrElse(Set.empty)
      )
    } yield (map, tacticGroups)
  }
}
