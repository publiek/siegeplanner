import java.nio.file.{FileSystems, Paths}

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.slick.javadsl.SlickSession
import akka.stream.alpakka.slick.scaladsl.Slick
import akka.stream.scaladsl._
import akka.util.ByteString
import com.typesafe.config.{Config, ConfigFactory}
import slick.jdbc.JdbcBackend
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object ImportScrapeResults extends App {
  implicit val system = ActorSystem()
  implicit val mat = ActorMaterializer()

  //Input validation
  if (args.length < 1) {
    Console.err.println("Usage [scrape result file] (scraper/results)")
    sys.exit(1)
  }

  lazy val fs = FileSystems.getDefault
  lazy val config: Config = ConfigFactory.load("application.conf")

  implicit val session: SlickSession = SlickSession.forConfig("slick.dbs.default", config)
  val db: JdbcBackend#DatabaseDef = session.db

  val result: Future[Done] = FileIO
    .fromPath(Paths.get(args(0)))
    .via(Framing.delimiter(ByteString("\n"), 1000000, true).map(_.utf8String))
    .runWith(Slick.sink(line => sqlu"INSERT INTO operator_ban VALUES(DEFAULT, ${line})"))

  result.map(_ => system.terminate())
}
