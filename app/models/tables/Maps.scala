package models.tables

import com.github.tototoshi.slick.PostgresJodaSupport._
import org.joda.time._
import models.CustomPostgresProfile.api._

case class MapRow(id: Int, name: String, slug: String, createdAt: DateTime, modifiedAt: DateTime)

class MapTable(tag: Tag) extends Table[MapRow](tag, "map") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def slug = column[String]("slug")
  def createdAt = column[DateTime]("created_at")
  def modifiedAt = column[DateTime]("modified_at")
  def * = (id, name, slug, createdAt, modifiedAt) <> ((MapRow.apply _).tupled, MapRow.unapply)
}

case class MapFloorRow(id: Int, level: Int, file: String, mapId: Int)

class MapFloorTable(tag: Tag) extends Table[MapFloorRow](tag, "map_floor") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def level = column[Int]("level")
  def file = column[String]("file")
  def mapId = column[Int]("map_id")
  def * = (id, level, file, mapId) <> ((MapFloorRow.apply _).tupled, MapFloorRow.unapply)
}
