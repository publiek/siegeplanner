package models.tables

import com.github.tototoshi.slick.PostgresJodaSupport._
import org.joda.time._
import models.CustomPostgresProfile.api._
import models.tables.Side.Side

case class OperatorRow(id: Int, name: String, icon: String, side: Side, createdAt: DateTime, modifiedAt: DateTime)

object Side extends Enumeration {
  type Side = Value
  val attacker, defender = Value
}

class OperatorTable(tag: Tag) extends Table[OperatorRow](tag, "operator") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def icon = column[String]("icon")
  def side = column[Side]("side")
  def createdAt = column[DateTime]("created_at")
  def modifiedAt = column[DateTime]("modified_at")
  def * = (id, name, icon, side, createdAt, modifiedAt) <> ((OperatorRow.apply _).tupled, OperatorRow.unapply)
}