package models.tables

import models.Coordinates
import models.CustomPostgresProfile.api._

case class TacticRow(id: Int, name: String, description: String, links: List[String], videoUrl: String, thumbnailUrl: String, operatorId: Int, tacticGroupId: Int)

class TacticTable(tag: Tag) extends Table[TacticRow](tag, "tactic") {
  def id: Rep[Int] = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name: Rep[String] = column[String]("name")
  def description: Rep[String] = column[String]("description")
  def links: Rep[List[String]] = column[List[String]]("links")
  def videoUrl: Rep[String] = column[String]("video_url")
  def thumbnailUrl: Rep[String] = column[String]("thumbnail_url")
  def operatorId: Rep[Int] = column[Int]("operator_id")
  def tacticGroupId: Rep[Int] = column[Int]("tactic_group_id")

  def * = (id, name, description, links, videoUrl, thumbnailUrl, operatorId, tacticGroupId) <>  ((TacticRow.apply _).tupled, TacticRow.unapply)
}

case class TacticGroupRow(id: Int, locationName: String, coordinates: Coordinates, mapFloorId: Int)

class TacticGroupTable(tag: Tag) extends Table[TacticGroupRow](tag, "tactic_group") {
  implicit val coordinatesListMapper = MappedColumnType.base[Coordinates, List[List[Int]]](
    tupleList => tupleList.map{case (a, b) => List(a, b)},
    list => list.map(l => (l.head, l.last))
  )

  def id: Rep[Int] = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def locationName: Rep[String] = column[String]("location_name")
  def areanName: Rep[String] = column[String]("area_name")
  def coordinates: Rep[Coordinates] = column[Coordinates]("coordinates")
  def mapFloorId: Rep[Int] = column[Int]("map_floor_id")

  def * = (id, locationName, coordinates, mapFloorId) <> ((TacticGroupRow.apply _).tupled, TacticGroupRow.unapply)
}

