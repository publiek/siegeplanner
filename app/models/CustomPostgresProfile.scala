package models

import com.github.tminglei.slickpg.{ExPostgresProfile, PgArraySupport, PgEnumSupport}
import models.tables.Side

trait CustomPostgresProfile extends ExPostgresProfile with PgArraySupport with PgEnumSupport {
  override val api = MyAPI

  object MyAPI extends API with ArrayImplicits {
    implicit val strListTypeMapper = new SimpleArrayJdbcType[String]("text").to(_.toList)
    // multi dimensional array support e.g. List[List[Int]]
    implicit val intIntWitness = ElemWitness.AnyWitness.asInstanceOf[ElemWitness[List[Int]]]
    implicit val simpleIntIntListTypeMapper = new SimpleArrayJdbcType[List[Int]]("int4[]")
      .to(_.asInstanceOf[Seq[Array[Any]]].toList.map(_.toList.asInstanceOf[List[Int]]))
    // operator enum support
    implicit val operatorSideEnumMapper = createEnumJdbcType("Side", Side, true)
  }
}

object CustomPostgresProfile extends CustomPostgresProfile