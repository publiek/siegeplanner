package object models {
  case class OperatorBanAggregation(map: String, operator: String, totalBans: Int)
  case class OperatorBan(operator: String, percentage: Double)

  case class MapOperatorBans(name: String, bans: List[OperatorBan])

  case class Operator(id: Int, name: String)

  type Coordinates = List[(Int, Int)]
}
