package models.repositories

import javax.inject.Inject
import models.tables.{TacticGroupRow, TacticGroupTable, TacticRow, TacticTable}
import models.{Coordinates, CustomPostgresProfile}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}

import scala.concurrent.{ExecutionContext, Future}

case class TacticGroup(id: Int, locationName: String, coordinates: Coordinates, mapFloorId: Int, tactics: Seq[TacticRow])

class TacticRepository @Inject()(val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[CustomPostgresProfile] {

  import dbConfig.profile.api._

  def findByTacticGroup(groupId: Int): Future[Option[(TacticGroupRow, Seq[TacticRow])]] = db.run(
    TableQuery[TacticGroupTable]
      .filter(_.id === groupId)
      .joinLeft(TableQuery[TacticTable]).on(_.id === _.tacticGroupId)
      .result
  ).map(
    rows => rows.groupBy(_._1.id).map {
      case (_, tactics) => (tactics.head._1, tactics.filter(_._2.isDefined).map(_._2.get))
    }.headOption
  )

  def findByMapFloor(floorIds: Set[Int], operatorId: Option[Int] = None): Future[Seq[TacticGroup]] = db.run(
    TableQuery[TacticGroupTable]
      .filter(_.mapFloorId.inSet(floorIds))
      .join(TableQuery[TacticTable]).on(_.id === _.tacticGroupId)
      .filterOpt(operatorId)(_._2.operatorId === _)
      .result
  ).map(groupResults)

  private def groupResults(rows: Seq[(TacticGroupRow, TacticRow)]): Seq[TacticGroup] = {
    rows.groupBy(_._1.id).map {
      case (_, tactics) =>
        val group: TacticGroupRow = tactics.head._1
        TacticGroup(group.id, group.locationName, group.coordinates, group.mapFloorId, tactics.map(_._2))
    }.toSeq
  }

  def save(group: TacticGroupRow): Future[Option[TacticGroupRow]] = db.run(
    (TableQuery[TacticGroupTable] returning TableQuery[TacticGroupTable]).insertOrUpdate(group)
  )

  def save(tactic: TacticRow): Future[Option[TacticRow]] = db.run(
    (TableQuery[TacticTable] returning TableQuery[TacticTable]).insertOrUpdate(tactic)
  )

  def delete(tacticId: Int): Future[Int] = db.run(
    TableQuery[TacticTable]
      .filter(_.id === tacticId)
      .delete
  )
}

