package models.repositories

import javax.inject.Inject
import models.CustomPostgresProfile
import models.tables.{MapFloorRow, MapFloorTable, MapRow, MapTable}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}

import scala.concurrent.{ExecutionContext, Future}

case class MapWithFloors(name: String, slug: String, floors: Seq[MapFloorRow])
case class FloorWithMap(floor:MapFloorRow, mapRow: MapRow)

class MapRepository @Inject()(val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[CustomPostgresProfile] {
  import dbConfig.profile.api._

  def findAll: Future[Seq[MapRow]] = db.run(
    TableQuery[MapTable].result
  )

  def findAllFloors: Future[Seq[FloorWithMap]] = db.run(
    TableQuery[MapFloorTable]
      .join(TableQuery[MapTable]).on(_.mapId === _.id)
      .result
    ).map(results => results.map(FloorWithMap.apply _ tupled)
  )

  def findBySlug(slug: String): Future[Option[MapWithFloors]] = db.run(
    TableQuery[MapTable]
      .filter(_.slug === slug)
      .join(TableQuery[MapFloorTable]).on(_.id === _.mapId)
      .result
    ).map(
      rows => rows.groupBy(_._1.id).map {
        case (_, floors) =>
          val map: MapRow = floors.head._1
          MapWithFloors(map.name, map.slug, floors.map(_._2))
      }.headOption
    )
}
