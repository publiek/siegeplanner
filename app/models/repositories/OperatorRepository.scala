package models.repositories

import javax.inject.Inject
import models.CustomPostgresProfile
import models.tables.{OperatorRow, OperatorTable}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}

import scala.concurrent.{ExecutionContext, Future}

class OperatorRepository @Inject()(val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[CustomPostgresProfile] {
  import dbConfig.profile.api._

  def findAll: Future[Seq[OperatorRow]] = db.run(
    TableQuery[OperatorTable].result
  )

  def save(row: OperatorRow): Future[Int] = db.run(
    TableQuery[OperatorTable].insertOrUpdate(row)
  )
}
