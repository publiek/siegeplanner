package models.repositories

import javax.inject.Inject
import models.{CustomPostgresProfile, MapOperatorBans, OperatorBan, OperatorBanAggregation}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.GetResult

import scala.concurrent.{ExecutionContext, Future}

class OperatorBanRepository @Inject()(val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[CustomPostgresProfile] {
  import dbConfig.profile.api._

  implicit val getOperatorBanResult = GetResult(r => OperatorBanAggregation(r.nextString(), r.nextString(), r.nextInt()))

  def get(since: Option[String] = None, limit: Int = 5): Future[List[MapOperatorBans]] = {
    val sinceDate = since.getOrElse("1970-01-01")
    val bansFuture = db.run(sql"""
      select
        json ->> 'map' as map,
        json ->> 'operator' as op,
        COUNT(*) as ban_count
      from operator_ban
      where TO_DATE(json ->> 'match_date', 'YYYY-MM-DD') > TO_DATE(${sinceDate}, 'YYYY-MM-DD')
      group by (map, op)
      order by ban_count desc
      """.as[OperatorBanAggregation])

    val matchesFuture = db.run(sql"""
      select
        json ->> 'map' as map,
        count(distinct json ->> 'match_id') as total_count
      from operator_ban
      where TO_DATE(json ->> 'match_date', 'YYYY-MM-DD') > TO_DATE(${sinceDate}, 'YYYY-MM-DD')
      group by (map)
      order by total_count desc;
      """.as[(String, Int)])

    for {
      bans <- bansFuture
      matches <- matchesFuture
    } yield matches.map {
      case (map, total) =>
        MapOperatorBans(
          map,
          bans.filter(map == _.map).take(limit).map(
            agg => OperatorBan(agg.operator, (agg.totalBans.toDouble / total.toDouble) * 100D)
          ).toList
        )
    }.toList
  }
}
