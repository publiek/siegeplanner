package controllers

import com.typesafe.scalalogging.LazyLogging
import javax.inject._
import models.repositories.OperatorRepository
import models.tables.OperatorRow
import org.joda.time.DateTime
import play.api.i18n.I18nSupport
import play.api.libs.json.{Json, OWrites, Writes}
import play.api.mvc.{Action, _}
import services.Utils

import scala.concurrent.ExecutionContext

@Singleton
class OperatorController @Inject()(
    val controllerComponents: ControllerComponents,
    operatorRepository: OperatorRepository
  )(implicit ec: ExecutionContext) extends BaseController with I18nSupport with LazyLogging {

  def operators(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    implicit val dateTimeWriter: Writes[DateTime] = Utils.jodaDateWrites("yyyy-MM-dd HH:mm:ss")
    implicit val operatorRowWrites: OWrites[OperatorRow] = Json.writes[OperatorRow]

    for {
      operators <- operatorRepository.findAll
    } yield Ok(Json.arr(operators.map(o => Json.toJson(o))))
  }
}
