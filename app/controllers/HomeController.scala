package controllers

import javax.inject._
import models.repositories.OperatorBanRepository
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.I18nSupport
import play.api.mvc._
import scala.concurrent.ExecutionContext

@Singleton
class HomeController @Inject()(
    val controllerComponents: ControllerComponents,
    opRepository: OperatorBanRepository
  )(implicit ec: ExecutionContext) extends BaseController with I18nSupport {

  val patchDates: Map[String, String] = Map(
    "Y4S4" -> "2019-12-04",
    "Y4S3" -> "2019-09-22",
    "Y4S2" -> "2019-06-11",
    "Y4S1" -> "2019-03-06"
  )
  val patches: Map[String, String] = Map(
    "Y4S4" -> "Y4S4 (Shifting tides)",
    "Y4S3" -> "Y4S3 (Reversed Friendly Fire)",
    "Y4S2" -> "Y4S2 (Phantom Sight)",
    "Y4S1" -> "Y4S1 (Burnt Horizon)"
  )
  val patchForm: Form[String] = Form(single("patch" -> text.verifying(t => patchDates.contains(t)) ))

  def index(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    val boundForm: Form[String] = patchForm.bindFromRequest()

    (if (boundForm.hasErrors) opRepository.get() else opRepository.get(patchDates.get(boundForm.get)))
        .map(matches => Ok(views.html.index(patchForm, patches, matches)))
  }
}
