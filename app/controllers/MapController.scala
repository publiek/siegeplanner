package controllers

import javax.inject._
import models.repositories.{MapRepository, TacticGroup, TacticRepository}
import models.tables.TacticRow
import play.api.i18n.I18nSupport
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.ExecutionContext

@Singleton
class MapController @Inject()(
    val controllerComponents: ControllerComponents,
    mapRepository: MapRepository,
    tacticRepository: TacticRepository
  )(implicit ec: ExecutionContext) extends BaseController with I18nSupport {

  def index(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    mapRepository.findAll.map(r => Ok(views.html.map.index(r)))
  }

  def map(mapSlug: String): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    mapRepository.findBySlug(mapSlug).map(map => {
      if (map.isDefined)
        Ok(views.html.map.map(map.get))
      else
        NotFound("Page not found")
    })
  }

  def tactics(mapSlug: String, operatorId: Option[Int] = None): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] => {
      for {
        floorIds <- mapRepository.findBySlug(mapSlug)
          .map(m => m
            .map(_.floors.map(_.id).toSet)
            .getOrElse(Set.empty)
          )
        tacticGroups <- tacticRepository.findByMapFloor(floorIds, operatorId)
      } yield Ok(Json.obj(
        tacticGroups.groupBy(_.mapFloorId).map {
          case (level, t) => (s"$level", Json.toJsFieldJsValueWrapper(t.map(tacticGroupToJson)))
        }.toSeq: _*))
    }
  }

  def tacticsByOperator(mapSlug: String, operatorId: Int): Action[AnyContent] = tactics(mapSlug, Some(operatorId))

  private def tacticGroupToJson(tacticGroup: TacticGroup): JsValue = {
    implicit val tacticRowWrites: OWrites[TacticRow] = Json.writes[TacticRow]

    Json.obj(
      "type" -> "Feature",
      "properties" -> Json.obj(
        "groupId" -> tacticGroup.id,
        "tactics" -> tacticGroup.tactics
          .zipWithIndex
          .map {
            case (tactic, index) => Json.toJson(tactic).as[JsObject] ++ Json.obj("index" -> index)
          }
      ),
      "geometry" -> Json.obj(
        "type" -> "Polygon",
        "coordinates" -> Json.arr(tacticGroup.coordinates.map{ case (a, b) => (a, b) })
      )
    )
  }
}
