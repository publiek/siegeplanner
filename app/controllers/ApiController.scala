package controllers

import com.typesafe.scalalogging.LazyLogging
import javax.inject._
import models.repositories.{MapRepository, MapWithFloors, TacticGroup, TacticRepository}
import models.tables.MapRow
import play.api.i18n.I18nSupport
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ApiController @Inject()(
    val controllerComponents: ControllerComponents,
    mapRepository: MapRepository,
    tacticRepository: TacticRepository
  )(implicit ec: ExecutionContext) extends BaseController with I18nSupport with LazyLogging {

  def maps(): Action[AnyContent] = Action.async {
    logger.info(s"Received call: maps")
    mapRepository.findAll.map(maps => Ok(Json.toJson(mapToJson(maps))))
  }

  private def mapToJson(maps: Seq[MapRow]): Seq[JsObject] = {
    maps.map {
      map =>
        Json.obj(
          "name" -> map.name,
          "slug" -> map.slug,
        )
    }
  }

  private def fetchTactics(mapSlug: String, operatorId: Option[Int] = None): Future[(Option[MapWithFloors], Seq[TacticGroup])] = {
    for {
      mapWithFloors <- mapRepository.findBySlug(mapSlug)
      tacticGroups <- tacticRepository.findByMapFloor(
        mapWithFloors.map(m => m.floors.map(_.id).toSet).getOrElse(Set.empty),
        operatorId
      )
    } yield (mapWithFloors, tacticGroups)
  }

  def tactics(mapSlug: String, operatorId: Int): Action[AnyContent] = Action.async {
    logger.info(s"Received call: tactics/$mapSlug/$operatorId")
    fetchTactics(mapSlug, Some(operatorId)).map {
      case (Some(mapWithFloors), tacticGroups) => Ok(Json.toJson(tacticGroups.flatMap(t => tacticToJson(t, mapWithFloors))))
      case _ => Ok( JsArray(Seq.empty))
    }
  }

  private def tacticToJson(tacticGroup: TacticGroup, mapWithFloors: MapWithFloors): Seq[JsObject] = {
    tacticGroup.tactics
      .map {
        tactic =>
          Json.obj(
            "name" -> tactic.name,
            "description" -> tactic.description,
            "thumbnailUrl" -> tactic.thumbnailUrl,
            "location" -> tacticGroup.locationName,
            "floorLevel" -> JsNumber(BigDecimal.apply(mapWithFloors.floors.find(_.id == tacticGroup.mapFloorId).map(_.level).getOrElse(0))),
            "operatorId" -> tactic.operatorId,
          )
      }
  }
}
