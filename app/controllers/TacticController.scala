package controllers

import com.typesafe.scalalogging.LazyLogging
import forms.TacticGroupForm
import forms.TacticGroupForm.TacticGroupData
import javax.inject._
import models.repositories.{MapRepository, OperatorRepository, TacticRepository}
import models.tables.TacticGroupRow
import play.api.data.Form
import play.api.i18n.I18nSupport
import play.api.mvc.{Action, _}
import services.providers.MapFloorProvider

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

@Singleton
class TacticController @Inject()(
    val controllerComponents: ControllerComponents,
    mapRepository: MapRepository,
    tacticRepository: TacticRepository,
    operatorRepository: OperatorRepository,
    mapFloorProvider: MapFloorProvider
  )(implicit ec: ExecutionContext) extends BaseController with I18nSupport with LazyLogging {

  private val saveUrl: Call = routes.TacticController.save()
  implicit val defaultTimeout: FiniteDuration = 1 minute

  def index(mapSlug: String): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    mapFloorProvider.fetchByMap(mapSlug).map {
      case (map, groups) => if (map.isDefined) {
        Ok(views.html.tactic.index(map.get, groups))
      } else {
        NotFound("Map not found")
      }
    }
  }

  def create: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    for {
      operators <- operatorRepository.findAll
      mapFloors <- mapRepository.findAllFloors
    } yield (Ok(views.html.tactic.edit(TacticGroupForm.form, operators, mapFloors, saveUrl)))
  }

  def edit(groupId: Int): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    for {
      group <- tacticRepository.findByTacticGroup(groupId)
      operators <- operatorRepository.findAll
      mapFloors <- mapRepository.findAllFloors
    } yield {
      if (group.isDefined) {
        Ok(views.html.tactic.edit(
          TacticGroupForm.form.fill(TacticGroupData(group.get._1, group.get._2)),
          operators,
          mapFloors,
          saveUrl)
        )
      } else {
        NotFound("Tactic group not found")
      }
    }
  }

  def save: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    for {
      operators <- operatorRepository.findAll
      mapFloors <- mapRepository.findAllFloors
    } yield {
      val errorFunction = { formWithErrors: Form[TacticGroupData] =>
        logger.warn("Form errors {}", formWithErrors)
        BadRequest(views.html.tactic.edit(formWithErrors, operators, mapFloors, saveUrl))
      }

      val successFunction = { data: TacticGroupData =>
        val savedGroup: Option[TacticGroupRow] = Await.result(tacticRepository.save(data.tacticGroup), defaultTimeout)
        // If it is updated it return a None if inserted it returns the object with id
        val groupId = savedGroup.map(_.id).getOrElse(data.tacticGroup.id)
        val (group, currentTactics) = Await.result(tacticRepository.findByTacticGroup(groupId), defaultTimeout).get

        val tactics = Await.result(Future.sequence {
          val updatedTactics = data.tactics.map(tactic => tactic.copy(tacticGroupId = group.id))
          val deletedTactics = currentTactics.diff(updatedTactics)

          deletedTactics.map(t => tacticRepository.delete(t.id)) ++ updatedTactics.map(tacticRepository.save)
        }, defaultTimeout)

        logger.info("Saved {} tactics to database", tactics.size)
        val route: Call = mapFloors.headOption
          .map(m => routes.TacticController.index(m.mapRow.slug))
          .getOrElse(routes.MapController.index())

        Redirect(route).flashing("message" -> s"Saved ${tactics.size} tactics successfully!")
      }

      val formValidationResult: Form[TacticGroupData] = TacticGroupForm.form.bindFromRequest
      formValidationResult.fold(errorFunction, successFunction)
    }
  }
}
