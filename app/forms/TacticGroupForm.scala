package forms

import models.tables.{TacticGroupRow, TacticRow}

object TacticGroupForm {
  import play.api.data.Forms._
  import play.api.data.Form

  case class TacticGroupData(tacticGroup: TacticGroupRow, tactics: Seq[TacticRow])

  val form: Form[TacticGroupData] = Form (
    mapping(
    "tacticGroup" -> mapping(
        "id" -> default(number, 0),
        "locationName" -> nonEmptyText,
        "coordinates" -> list(tuple("x" -> number, "y" -> number)),
        "mapFloorId" -> number,
        )(TacticGroupRow.apply)(TacticGroupRow.unapply),
    "tactics" -> seq(mapping(
        "id" -> default(number, 0),
        "name" -> nonEmptyText,
        "description" -> nonEmptyText,
        "links" -> list(text),
        "videoUrl" -> nonEmptyText,
        "thumbnailUrl" -> nonEmptyText,
        "operatorId" -> number,
        "tacticGroupId" -> default(number, 0),
      )(TacticRow.apply)(TacticRow.unapply))
    )(TacticGroupData.apply)(TacticGroupData.unapply)
  )
}
