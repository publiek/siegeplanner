import sbt._

name := """siegeplanner"""
organization := "com.timattic"
scalaVersion := "2.12.8"
version := "1.0-SNAPSHOT"

val playSlickVersion = "5.0.0"
val slickVersion = "3.3.2"
val akkaVersion = "2.5.23"
val slickPgVersion = "0.19.0"

lazy val databaseUrl = sys.env.getOrElse("DB_URL", "jdbc:postgresql://127.0.0.1:5432/siegeplanner?stringtype=unspecified")
lazy val databaseUser = sys.env.getOrElse("DB_USER", "user")
lazy val databasePassword = sys.env.getOrElse("DB_PASSWORD", "koffie")

lazy val root = (project in file("."))
  .settings(
    libraryDependencies += guice,
    libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
    // Slick
    libraryDependencies += "com.typesafe.slick" %% "slick" % slickVersion,
    libraryDependencies += "com.typesafe.play" %% "play-slick" % playSlickVersion,
    libraryDependencies += "com.typesafe.play" %% "play-slick-evolutions" % playSlickVersion,
    // DB migrations
    libraryDependencies += "org.flywaydb" % "flyway-core" % "5.1.4",

    // Extendend postgres data types
    libraryDependencies += "com.github.tminglei" %% "slick-pg" % slickPgVersion,

    // Akka slick for importing records with streaming
    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    libraryDependencies += "com.lightbend.akka" %% "akka-stream-alpakka-slick" % "1.1.0",

    libraryDependencies += "com.github.tototoshi" %% "slick-joda-mapper" % "2.3.0",
    libraryDependencies += "com.adrianhurt" %% "play-bootstrap" % "1.5.1-P27-B4",

    flywayUrl := databaseUrl,
    flywayUser := databaseUser,
    flywayPassword := databasePassword,
    flywayLocations := Seq("filesystem:conf/migration"),
  ).enablePlugins(FlywayPlugin, PlayScala)
