## Siegeplanner

### Database

Setup dabatase 

```
docker-compose up -d 
```

To create the dabatase and schema run 

```
sbt flywayMigrate
```

Import the scraped data into the database 

```
sbt "runMain ImportScrapeResults scraper/results/siege_gg_bans.json" 
```


### Application

First start database see above and then run

```
sbt run
```

Visit http://localhost:9000 for the site

#### Frontend 

Uses jQuery with Bootstrap with custom overwrites in *.css and *.js see directory public/

Html (twirl) can be found under app/views/*scala.html 

Twirl is scala code with html see also https://www.playframework.com/documentation/2.8.x/ScalaTemplates
 
 